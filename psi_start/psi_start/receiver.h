#pragma once
#include "communicator.h"

class receiver : public communicator {
public:
	receiver(int tp, int lp, int dataspeed) :communicator(tp, lp) {
		communicator::dataspeed = dataspeed;
		//before receiving head package
		communicator::window = 1;
	}
	head received_head;
	list<packet> &receive(int waiting_ms);
	void send_file_assertion(bool ok);
	void send_head_ack();
protected:
	void send_ack(vector<int> indc);
};
