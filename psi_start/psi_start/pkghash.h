#pragma once

#include <string>
#include <list>
#include "protocol.h"
#include "hl_md5wrapper.h"

std::string get_pkgs_hash(std::list<packet> pkgs);