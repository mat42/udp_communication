#include "receiver.h"

static void link_package(list<packet> *pkgs, packet &pkg) {
	if (pkgs->size() == 0) {
		pkgs->push_back(pkg);
		return;
	}
	auto i = pkgs->cend();
	do {
		i--;
		if (pkg.idx == i->idx) {
			i = pkgs->erase(i);
			pkgs->emplace(i, pkg);
			return;
		}
		if (i->idx < pkg.idx) {
			pkgs->emplace(next(i), pkg);
			return;
		}
	} while (i != pkgs->cbegin());
	pkgs->emplace(pkgs->begin(), pkg);
}

list<packet> &receiver::receive(int waiting_ms) {

	uint32_t crc_rec;
	uint32_t crc_comp;

	set_waiting_time(waiting_ms);

	printf("Waiting for datagram ...\n");

	list<packet> *pcks = new list<packet>();

	/****************Fetching head************/
	byte* head_buf;
	vector<int> head_ack;
	head head;
	do {
		do {
			head_buf = receive_bytes(HEAD_LEN);
		} while (head_buf == nullptr);

		crc_comp = crc32_fast(head_buf, HEAD_LEN - sizeof(crc_comp));
		memcpy(&crc_rec, head_buf + (HEAD_LEN - sizeof(crc_rec)), sizeof(crc_rec));
	} while (crc_rec != crc_comp);

	printf("Head fetched!\n");

	set_waiting_time(300);

	memcpy(&head, head_buf, HEAD_LEN - sizeof(crc_rec));

	window = head.window;
	received_head = head;

	send_head_ack();

	vector<int> exp_pcks;
	for (int i = 1; i <= window && i <= head.packnum; i++)
		exp_pcks.push_back(i);

	int next = window + 1;
	int timeout_cntr = 0;
	while (next <= head.packnum && timeout_cntr < 100) {
		for (int i = 0; i < window; i++) {
			/************Fetching package*************/
			byte *pck_buf = receive_bytes(PACKAGE_LEN);
			if (pck_buf == nullptr) {
				printf("TIMEOUT\n");
				timeout_cntr++;
				break;
			}
			else
				timeout_cntr = 0;
			/**************Unpack package****************/
			packet pck;
			memcpy(&pck, pck_buf, PACKAGE_LEN - sizeof(crc_rec));
			/***********checking CRC*************/
			memcpy(&crc_rec, pck_buf + (PACKAGE_LEN - sizeof(crc_rec)), sizeof(crc_rec));
			crc_comp = crc32_fast(pck_buf, PACKAGE_LEN - sizeof(crc_comp));
			if (crc_comp == crc_rec) {
				//remove if packet allready exist
				vector<int>::const_iterator ex_idx = find(exp_pcks.cbegin(), exp_pcks.cend(), pck.idx);

				if (ex_idx != exp_pcks.cend()) {
					link_package(pcks, pck);
					exp_pcks.erase(ex_idx);
					if (exp_pcks.empty() && next >= head.packnum)
						break;
					if (next < head.packnum)
						exp_pcks.push_back(next++);
				}
				else {
					printf("Noone expected this package (%d)!\n", pck.idx);
				}
			}
		}
		if (exp_pcks.empty() && next >= head.packnum) {
			printf("(All) package(s) FINE!\n");
			break;
		}
		send_ack(exp_pcks);
	}
	int pkg_cnt = pcks->empty() ? 0 : head.packnum - 1 - pcks->size();
	printf("Receiving finished: %d lost\n", pkg_cnt);
	for (packet p : *pcks) {
		printf("%d ", p.idx);
	}
	printf("\n");

	return *pcks;
}


static void add_crc32(char *buf) {
	uint32_t crc = crc32_fast(buf, PACKAGE_LEN - sizeof(crc));
	memcpy(buf + (PACKAGE_LEN - sizeof(crc)), &crc, sizeof(crc));
}

void receiver::send_head_ack() {
	byte *bytes = get_bytes(sizeof(ack_head), (byte *)&dataspeed, 4);
	sendto(socketS, (char *)bytes, sizeof(ack_head), 0, (sockaddr*)&addrDest, sizeof(addrDest));
	printf("Head ack sent\n");
}

void receiver::send_ack(vector<int> idxs) {

	ack ack{ idxs.size() };
	char ack_buf[PACKAGE_LEN];

	//to avoid all-zeros package
	for (int i = 0; i < ACK_DATALEN; i++)
		ack.indices[i] = ACK_IDX_FILL;

	if (ack.cnt != 0) {
		memcpy(&ack.indices, idxs.data(), ack.cnt * sizeof(uint32_t));
	}

	memcpy(ack_buf, &ack, PACKAGE_LEN - sizeof(uint32_t));
	add_crc32(ack_buf);
	sendto(socketS, ack_buf, PACKAGE_LEN, 0, (sockaddr*)&addrDest, sizeof(addrDest));
	printf("Ack sent");
	for (int i = 0; i < idxs.size(); i++)
		printf(" %d", idxs[i]);
	printf("\n");
}

void receiver::send_file_assertion(bool ok) {
	byte ack_bytes[PACKAGE_LEN];
	for (int i = 0; i < PACKAGE_LEN; i++)
		ack_bytes[i] = ok ? TRUE_FILE_BYTE : FALSE_FILE_BYTE;

	sendto(socketS, (char*)&ack_bytes, PACKAGE_LEN, 0, (sockaddr*)&addrDest, sizeof(addrDest));

	byte *stopBytes;
	byte *ofBytes;
	while ((stopBytes = receive_bytes(PACKAGE_LEN)) == nullptr
		|| !is_of_element(stopBytes, PACKAGE_LEN, STOP_BYTE)) {
		printf("Sending file ack");
		sendto(socketS, (char*)&ack_bytes, PACKAGE_LEN, 0, (sockaddr*)&addrDest, sizeof(addrDest));
	}
}
