#include "filefrag.h"

list<packet> &fragment(char *buf, int len);

list<packet> &get_file_packages(string fpath) {

	FILE *fo = fopen(fpath.data(), "rb");
	if (fo == nullptr) {
		printf("File not found!\n");
		return *new list<packet>();;
	}

	fseek(fo, 0, SEEK_END);
	int len = ftell(fo);
	rewind(fo);

	char *buf = new char[len];

	int read = fread(buf, len, 1, fo);
	if (!read) {
		printf("Error while reading file!");
		return *new list<packet>();
	}

	fclose(fo);

	return fragment(buf, len);
}

list<packet> &fragment(char *buf, int len) {
	list<packet> *pkgs = new list<packet>();
	int pkg_cnt = len / PKG_DATALEN + 1;
	for (int i = 0; i != pkg_cnt; i++) {

		packet *pkg = new packet();
		pkg->datalen = i == pkg_cnt - 1 ? len % PKG_DATALEN : PKG_DATALEN;
		pkg->idx = i + 1;

		int start = i * PKG_DATALEN;
		for (int j = 0; j < pkg->datalen; j++) {
			pkg->bytes[j] = buf[start + j];
		}

		pkgs->push_back(*pkg);
	}
	return *pkgs;
}

void to_file(list<packet> pkgs, string fpath) {
	if (pkgs.empty()) {
		printf("Nothing to save!\n");
		return;
	}
	FILE *fo = fopen(fpath.data(), "wb");
	int buflen = (pkgs.size() - 1) * PKG_DATALEN + pkgs.back().datalen;
	char *buf = new char[buflen];
	int cnt = 0;
	for (auto pkg : pkgs) {
		for (int i = 0; i < pkg.datalen; i++) {

			buf[cnt * PKG_DATALEN + i] = pkg.bytes[i];
		}
		cnt++;
	}
	fwrite(buf, buflen, 1, fo);
	printf("File saved!\n");
	fclose(fo);
}
