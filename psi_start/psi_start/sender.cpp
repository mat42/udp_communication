#include "sender.h"

bool sender::receive_ack_head() {
	byte *bytes = receive_bytes(sizeof(ack_head));
	if (bytes == nullptr)
		return false;

	ack_head ack;
	memcpy(&ack, bytes, sizeof(ack_head));

	if (is_of_element(bytes, DUPLICATE_LEN, ack.speed[0]))
		return false;

	dataspeed = ack.speed[0];
	return true;
}

void sender::send(list<packet> pkgs, head head_pkg, int waiting_ms) {
	set_waiting_time(waiting_ms);

	bool file_asserted = false;

	do {
		uint32_t crc;
		int cnt = 0;

		char head_bytes[HEAD_LEN];
		memcpy(head_bytes, &head_pkg, HEAD_LEN - sizeof(crc));
		crc = crc32_fast(head_bytes, HEAD_LEN - sizeof(crc));
		memcpy(head_bytes + (HEAD_LEN - sizeof(crc)), &crc, sizeof(crc));

		do {
			sendto(socketS, head_bytes, HEAD_LEN, 0, (sockaddr*)&addrDest, sizeof(addrDest));
			cnt++;
		} while ((!receive_ack_head() || dataspeed == 0) && cnt < 100);
		if (cnt >= 100) {
			perror("Failed to receive head ack\n");
			return;
		}

		int *to_send = new int[window];
		int next = 1;
		int max_conf = 0;

		for (int i = 0; i < window; i++, next++) {
			if (next < head_pkg.packnum)
				to_send[i] = next;
			else
				to_send[i] = 0;
		}
		while (1) {

			char pkg_bytes[PACKAGE_LEN];
			printf("Next : %i / %i\n", next, head_pkg.packnum);
			printf("Sending package(s)");
			clock_t start = std::clock();
			for (int i = 0; i < window && to_send[i] != 0; i++) {
				memcpy(pkg_bytes, &(*std::next(pkgs.begin(), to_send[i] - 1)), PACKAGE_LEN - sizeof(crc));
				crc = crc32_fast(pkg_bytes, PACKAGE_LEN - sizeof(crc));
				memcpy(pkg_bytes + (PACKAGE_LEN - sizeof(crc)), &crc, sizeof(crc));
				sendto(socketS, pkg_bytes, PACKAGE_LEN, 0, (sockaddr*)&addrDest, sizeof(addrDest));
				if (i % GROUP_SIZE == GROUP_SIZE - 1)
					Sleep(GROUP_TIMEOUT);
				printf(" %d", to_send[i]);
				to_send[i] = 0;
			}
			printf("\n");

			byte *packet_ack_bytes;
			cnt = 0;
			do {
				packet_ack_bytes = receive_bytes(PACKAGE_LEN);
				cnt++;
				if (cnt == 100) {
					perror("Failed to receive packet ack\n");
					return;
				}
			} while (packet_ack_bytes == nullptr);

			int duration(1000 * (std::clock() - start) / (double)CLOCKS_PER_SEC);

			int dif = window * 1000 / dataspeed - duration;

			if (dif > 0)
				Sleep(dif);
			else {
				printf("Too slow connection\n");
				dif = 0;
			}

			if (duration != 0) {
				printf("Current/Maximum sending speed %1.3f / %1.3f KiB/s\n", window *  1000. / (duration + dif), window * 1000. / duration);
			}
			else
				printf("Sending speed: default\n");

			if (is_of_element(packet_ack_bytes, PACKAGE_LEN, TRUE_FILE_BYTE)
				|| is_of_element(packet_ack_bytes, PACKAGE_LEN, FALSE_FILE_BYTE)) {
				file_asserted = is_of_element(packet_ack_bytes, PACKAGE_LEN, TRUE_FILE_BYTE);
				break;
			}
			else {
				uint32_t crc_rec;
				uint32_t crc_comp;
				ack packet_ack;
				memcpy(&packet_ack, packet_ack_bytes, PACKAGE_LEN - sizeof(crc_rec));
				/***********checking CRC*************/
				memcpy(&crc_rec, packet_ack_bytes + (PACKAGE_LEN - sizeof(crc_rec)), sizeof(crc_rec));
				crc_comp = crc32_fast(packet_ack_bytes, PACKAGE_LEN - sizeof(crc_comp));


				cnt = 0;
				if (crc_comp == crc_rec && max_conf <= packet_ack.indices[0]) {
					cnt = packet_ack.cnt;
					max_conf = packet_ack.indices[0];
					if (next <= packet_ack.indices[cnt - 1])
						next = packet_ack.indices[cnt - 1] + 1;
					printf("     Received : ");
					for (int i = 0; i < cnt; i++) {
						to_send[i] = packet_ack.indices[i];
						printf(" %i", packet_ack.indices[i]);
					}
					printf("\n");
				}
				for (int i = cnt; i < window && next < head_pkg.packnum; i++, next++) {
					to_send[i] = next;
				}
			}



		}

		byte stop_byte = STOP_BYTE;
		for (int i = 0; i < (window/10 > 10 ? window/10 : 10); i++) {
			printf("Sending stop packet\n");
			sendto(socketS, (char *)get_bytes(PACKAGE_LEN, &stop_byte, 1), PACKAGE_LEN, 0, (sockaddr*)&addrDest, sizeof(addrDest));
			Sleep(1);
		}

	} while (!file_asserted);

	printf("Packages are sent\n");
}