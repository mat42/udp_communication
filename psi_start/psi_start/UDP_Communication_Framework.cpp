// UDP_Communication_Framework.cpp : Defines the entry point for the console application.
//

#pragma comment(lib, "ws2_32.lib")
#include "stdafx.h"
#include <winsock2.h>
#include "ws2tcpip.h"
#include <stdio.h>

#include <iostream>
#include <sstream>
#include "protocol.h"
#include "filefrag.h"
#include "communicator.h"
#include "pkghash.h"
#include "sender.h"
#include "receiver.h"


void send() {
	sender sender(8888, 5555, 10);

	if (!sender.socket_inited)
		return;

	string fname = "Test_Cat.jpg";
	list<packet> pkgs = get_file_packages(fname);
	head head_packet;
	string hash = get_pkgs_hash(pkgs);

	head_packet.idx = 0;
	head_packet.packnum = pkgs.size() + 1;
	head_packet.window = sender.window;
	memcpy(&head_packet.fname, fname.data(), fname.size());

	memcpy(&head_packet.hash, hash.data(), hash.size());

	sender.send(pkgs, head_packet, 1000);
}

void receive() {
	cout << "Input speed of transmission (KiB/s): " << endl;

	int dataspeed;
	cin >> dataspeed;

	//receiver rec(5555, 8888, dataspeed);
	receiver rec(7777, 6666, dataspeed);

	if (!rec.socket_inited)
		return;

	clock_t start;
	list<packet> pkgs;
	bool file_received = true;
	do {
		start = std::clock();
		pkgs = rec.receive(100000);
		if (pkgs.size() == 0) {
			perror("Failed to receive packages!");
			return;
		}
		//break;
		string hash = get_pkgs_hash(pkgs);
		string rec_hash(rec.received_head.hash);
		rec_hash.resize(hash.size());
		if (hash != rec_hash) {
			printf("Hash failed\n");
			rec.send_file_assertion(false);
			printf("False file assertion sent\n");
			file_received = false;
		}
	} while (!file_received);

	rec.send_file_assertion(true);
	string fname(rec.received_head.fname);
	to_file(pkgs, fname);

	int duration(1000 * (std::clock() - start) / (double)CLOCKS_PER_SEC);
	printf("Duration %.3f\n", duration / 1000.);
}


int main()
{
	/*while (1) {
		receive();
	}*/
	send();

	getchar();
	return 0;
}
