#pragma once

#include <stdint.h>

//HEAD_LEN 299
#define HEAD_LEN 303

typedef struct head {
	uint32_t idx;
	uint32_t packnum;
	uint32_t window;
	char fname[255] = {};
	char hash[32] = {};
};

#define PACKAGE_LEN 1024
#define PKG_DATALEN 1012

#define STOP_BYTE 13
#define TRUE_FILE_BYTE 42

typedef unsigned char byte;

typedef struct packet {
	/* 
	 * packages may come reordered, hense idx
	 */
	uint32_t idx;
	uint32_t datalen;
	byte bytes[PKG_DATALEN];
};

#define ACK_DATALEN 254
#define ACK_IDX_FILL 33
typedef struct ack {
	//shall not be more then current window
	int32_t cnt;
	uint32_t indices[ACK_DATALEN];
};

#define TRUE_FILE_BYTE 42

#define FALSE_FILE_BYTE 66

#define STOP_BYTE 13

//CRC-free:
#define DUPLICATE_LEN 50

typedef struct ack_head{
	//speed = 0 if false assertion
	int32_t speed[DUPLICATE_LEN];
};


