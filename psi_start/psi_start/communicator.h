#pragma once

#include <winsock2.h>
#include "ws2tcpip.h"

#include "Crc32.h"
#include <ctime>

#include <list>
#include <string>
#include <vector>
#include <algorithm>

#include "stdafx.h"
#include "protocol.h"

#define TARGET_IP "147.32.217.8"

#define DEFAULT_SPEED 10 // (KiB/s)
#define WINDOW_SIZE 5
#define GROUP_SIZE 1
#define GROUP_TIMEOUT 4

using namespace std;

class communicator {

public:
	int target_port;
	int local_port;
	int dataspeed;
	int window;

	bool socket_inited;

	communicator(int tp, int lp) :target_port(tp), local_port(lp) {
		socket_inited = init_socket();
		dataspeed = DEFAULT_SPEED;
	}

protected:
	SOCKET socketS;
	struct sockaddr_in from;
	sockaddr_in addrDest;

	bool init_socket();
	byte *receive_bytes(int len);
	void set_waiting_time(DWORD msec);
	bool is_of_element(uint32_t *elements, int len, uint32_t element);
	bool is_of_element(byte *bytes, int len, byte element );
	byte *get_bytes(int len, byte *element, int element_len);
	~communicator() {
		closesocket(socketS);
	}
};
