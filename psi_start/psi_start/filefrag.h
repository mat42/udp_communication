#pragma once
#include "protocol.h"
#include <stdio.h>
#include <list>
#include <string>

using namespace std;

list<packet>& get_file_packages(string fpath);

void to_file(list<packet> pkgs, string fpath);
