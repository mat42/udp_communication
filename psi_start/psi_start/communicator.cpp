#pragma once
#include "communicator.h"

void InitWinsock()
{
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);
}

bool communicator::init_socket() {

	InitWinsock();

	/*****socket*****/
	struct sockaddr_in local;

	local.sin_family = AF_INET;
	local.sin_port = htons(local_port);
	local.sin_addr.s_addr = INADDR_ANY;

	socketS = socket(AF_INET, SOCK_DGRAM, 0);
	if (bind(socketS, (sockaddr*)&local, sizeof(local)) != 0) {
		printf("Binding error!\n");
		return false;
	}

	/********destination*********/
	addrDest.sin_family = AF_INET;
	addrDest.sin_port = htons(target_port);
	InetPton(AF_INET, _T(TARGET_IP), &addrDest.sin_addr.s_addr);

	return true;
}

void communicator::set_waiting_time(DWORD msec) {
	DWORD val = msec;
	setsockopt(socketS, SOL_SOCKET, SO_RCVTIMEO, (const char*)&val, sizeof DWORD);
	/*fd_set fds;
	int n;
	struct timeval tv;

	// Set up the file descriptor set.
	FD_ZERO(&fds);
	FD_SET(socketS, &fds);

	// Set up the struct timeval for the timeout.
	tv.tv_sec = 10;
	tv.tv_usec = 0;

	// Wait until timeout or data received.
	n = select(socketS, &fds, NULL, NULL, &tv);
	if (n == 0)
	{
		printf("Timeout..\n");
	}
	else if (n == -1)
	{
		printf("Error while setting timeout\n");
	}*/
}

byte* communicator::get_bytes(int len, byte *element, int element_len) {
	byte *bytes = new byte[len];
	for (int i = 0; i < len / element_len; i++)
		memcpy(bytes + i * element_len, element, element_len);
	return bytes;
}

#define ERROR_CNT 5

//TODO: replace with macro
//TODO: write narmal method for it
bool communicator::is_of_element(uint32_t *elements, int len, uint32_t element) {
	int error_cnt = 0;
	for(int i = 0; i < len; i++)
		if (elements[i] != element) {
			error_cnt++;
			if (error_cnt == ERROR_CNT)
				return false;
		}
	return true;
}

bool communicator::is_of_element(byte *bytes, int len, byte element) {
	int error_cnt = 0;
	for (int i = 0; i < len; i++)
		if (bytes[i] != element) {
			error_cnt++;
			if (error_cnt == ERROR_CNT)
				return false;
		}
	return true;
}

byte * communicator::receive_bytes(int len) {
	byte o = 0;
	byte *pkg_buf = get_bytes(len, &o, 1);

	int fromlen = sizeof(from);

	int res = recvfrom(socketS, (char*)pkg_buf, len, 0, (sockaddr*)&from, &fromlen);

	if (res == SOCKET_ERROR) {
		printf("Socket error!(Timed out)\n");
		return nullptr;
	}

	if (is_of_element(pkg_buf, len, 0)) {
		printf("Nothing received!");
		return nullptr;
	}

	return pkg_buf;
}
