#pragma once
#include "communicator.h"

class sender : public communicator {
public:
	sender(int tp, int lp, int window) :communicator(tp, lp) {
		communicator::window = window;
	}
	void send(list<packet> pkgs, head head_pkg, int waiting_ms);
	bool receive_ack_head();
};
